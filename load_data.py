import gc
import os
import pandas as pd
import time
from functions import write_stats
from clean_data import clean_data
import json
import pyarrow.parquet as pq
import variables
import checks

count = 0
directory = "data"  # Folder where the JSON files are
chunk_size = 16  # Number of JSON files to save to one temporary parquet file
tweets = []
t = time.time()
begin_time = int(t * 1000)  # Get the current time as begin time to calculate stats (seconds took and seconds to go)
error_count = 0
real_begin_time = begin_time
tweet_count = 0
errors = []
save_directory = "parquet"  # Folder to save the temporary parquet files
error_count_iteration = 0  # Number of errors in iteration (= 16 JSON files)
print("Loading and cleaning data...")
# Loop over each JSON file
for path in os.listdir(directory):
    count += 1
    with open(f"{directory}/{path}") as f:
        # Loop over each line in the JSON file
        for line in f.readlines():
            try:  # If the line is in JSON format give it to the clean function
                json_line = json.loads(line)
                json_line["json_file"] = path
                tweets.append(clean_data(json_line))  # Append the cleaned line to a list in order to create parquet file
                tweet_count += 1
            except Exception as e:  # Error in line, if things go normal then these errors are deleted tweets and "Exceeded" API error
                error_count += 1
                error_count_iteration += 1
                errors.append(f"Error {e} in line {line}")
    if count % chunk_size == 0 or count == 567:  # If the code did clean chunksize (16) number of JSONs then we created a parquet file from those 16 JSON files
        df_clean = pd.DataFrame(tweets).astype(str)  # Create a dataframe with all the clean tweets (all values as string to avoid errors)
        tweets = []
        t = time.time()
        end_time = int(t * 1000)  # Get the current time as end time to calculate stats (seconds took and seconds to go)
        time_took = (end_time - begin_time)/1000
        df_clean.to_parquet(f"{save_directory}/{count}.parquet")
        del df_clean  # Remove large dataframe from memory
        gc.collect()
        print(f"File {count}.parquet saved. Iteration took {time_took:.2f} seconds ({(((567-count)/chunk_size)*time_took):.2f} seconds to go) ({error_count_iteration} errors in iteration, {len(errors)} errors total)")
        error_count_iteration = 0  # Reset number of errors in iteration (16 JSON files)
        begin_time = end_time

print(f"Done! {tweet_count} tweets and {error_count} total of errors (see stats/{end_time}.txt)")

# Combine parquets
save_directory_database = "database"
files = []
print("Combining parquets...")
for path in os.listdir(save_directory):  # Append all parquet file names to a list
    if path != ".DS_Store":  # This is a macOS hidden file
        files.append(f"{save_directory}/{path}")

with pq.ParquetWriter(f"{save_directory_database}/airlines_db.parquet", schema=pq.ParquetFile(files[0]).schema_arrow) as writer:
    for file in files:
        writer.write_table(pq.read_table(file, schema=pq.ParquetFile(files[0]).schema_arrow))  # Combine all parquet files to one parquet file
json_sizes = []
print("Calculating some stats...")
write_stats(begin_time=real_begin_time, end_time=end_time, count=count, chunk_size=chunk_size, save_path=save_directory, tweet_count=tweet_count, errors=errors)
for f in os.listdir("data"):  # Calculating the sizes of all JSON file (for stats)
    file = os.stat(f"data/{f}")
    json_sizes.append(file.st_size / (1024 * 1024))

# Calculate some values just for stats and checks
size_json_folder = sum(json_sizes)
database_file = os.stat(f"{save_directory_database}/airlines_db.parquet")
database_size = database_file.st_size / (1024 * 1024)
percentage_decrease = (size_json_folder - database_size)/size_json_folder * 100
df_stats = pd.read_parquet("stats/stats.parquet")
df_stats["original_size_MB"] = size_json_folder
df_stats["parquet_database_size_MB"] = database_size
database = pd.read_parquet("database/airlines_db.parquet")
df_stats["percentage_english"] = (len(database[database["lang"] == "en"].index)/len(database.index))*100
df_stats["tweet_count_database"] = len(database.index)
for airline in variables.AIRLINES.items():
    df_stats[f"count_{airline[0]}_mentioned"] = len(database[database[f"{airline[0]}_mentioned"] == "1"].index)
df_stats.to_parquet("stats/stats.parquet")
del database

print(f"Saved successfully! From {size_json_folder:.2f} MB to {database_size:.2f} MB ({percentage_decrease:.2f}% decrease) ({(size_json_folder/database_size):.2f}x)")
checks.check_files()  # Function to checks if there are no tweets lost in the entire load data process
