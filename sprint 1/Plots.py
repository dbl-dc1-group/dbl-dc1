import gc
import warnings
import variables

import matplotlib as matplotlib
import numpy as np
import pandas as pd
import sqlite3
from dateutil.parser import parse

import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

conn = sqlite3.connect("../database/tweets.db")

# set default plot specifics
plt.rcParams["figure.figsize"] = [10, 5]
plt.rcParams["lines.markeredgewidth"] = 1

# ignore irrelevant warnings
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)


# --------------------------------- Tweets per Language ----------------------------------------------------------------
# extract tweets per language
query_plot_en = """
SELECT lang, count(lang) AS amount
FROM tweets
where lang != "und"
GROUP BY lang
ORDER BY amount DESC
"""

df_plot_en = pd.read_sql_query(query_plot_en, conn)

# create bar plots (with and without cut-off to see disregard English)
fig, ax = plt.subplots(nrows=2, squeeze=False, figsize=(20, 12))
fig.suptitle("amount of tweets per language", size=20, weight="bold")

sns.barplot(x=df_plot_en["lang"], y=df_plot_en["amount"], ax=ax[0,0])
ax[0,0].set_title("without a cut-off", size=16, weight="bold")
ax[0,0].set_xlabel("Language", size=13)
ax[0,0].set_ylabel("amount", size=13)

sns.barplot(x=df_plot_en["lang"], y=df_plot_en["amount"], ax=ax[1,0])
ax[1,0].set_ylim(0, 420000)
ax[1,0].set_title("cut-off at y=420000", size=16, weight="bold")
ax[1,0].set_xlabel("Language", size=13)
ax[1,0].set_ylabel("amount", size=13)

plt.subplots_adjust(hspace=0.3)

# show and save the plot
plt.show()
fig.get_figure().savefig("../figures/Tweets per language.svg")

# free memory space
del df_plot_en
gc.collect()


# ----------------------------- Tweets per month sent to our airlines --------------------------------------------------
# extract tweets sent to airlines (and total ones) per month
query_created_at = """
SELECT SUBSTRING("created_at", 5, 3) AS "month", COUNT("month") AS total
FROM tweets
GROUP BY "month"
"""

query_created_at1 = """
SELECT SUBSTRING("created_at", 5, 3) AS "month2", COUNT("month") AS British_Airways
FROM tweets
WHERE British_Airways_mentioned == 1
GROUP BY "month2"
"""

query_created_at2 = """
SELECT SUBSTRING("created_at", 5, 3) AS "month3", COUNT("month") AS AmericanAir
FROM tweets
WHERE AmericanAir_mentioned == 1
GROUP BY "month3"
"""

df_created_at = pd.read_sql_query(query_created_at, conn)
df_created_at1 = pd.read_sql_query(query_created_at1, conn)
df_created_at2 = pd.read_sql_query(query_created_at2, conn)

# add all information into 1 dataframe
df_created_at["British_Airways"] = df_created_at1["British_Airways"].copy()
df_created_at["AmericanAir"] = df_created_at2["AmericanAir"].copy()

# free memory space
del df_created_at1
del df_created_at2
gc.collect()

# reindex dataframe (May 2019 - Mar 2020)
months = ["May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan", "Feb", "Mar"]
df_created_at = df_created_at.set_index("month").reindex(months)

# create line plot
plot = df_created_at.plot(kind="line")
plt.title("Tweets per month to airlines vs total (May 2019 - March 2020)", size=16, weight="bold")
plt.xlabel("month", size=13)
plt.ylabel("amount", size=13)

# set x-axis ticks
plt.xticks(np.arange(len(months)), months)
plt.tight_layout()

# show and save the plot
plt.show()
plot.get_figure().savefig("../figures/Tweets per month.svg")

# free memory space
del df_created_at
gc.collect()


# ----------------------------- Tweets per hour sent to and sent by our airlines ---------------------------------------
# extract tweets per hour of day sent to and sent by our airlines
query_tweets_by_ba = """
SELECT SUBSTRING("created_at", 12, 2) AS "hour", count("hour") AS amount2
FROM tweets
where user_id_str = "18332190"
Group By "hour"
"""

query_tweets_at_ba = """
SELECT SUBSTRING("created_at", 12, 2) AS "hour2", count("hour2") AS amount3
FROM tweets
where British_Airways_mentioned == 1
Group By "hour2"
"""

query_tweets_by_aa = """
SELECT SUBSTRING("created_at", 12, 2) AS "hour3", count("hour3") AS amount4
FROM tweets
where user_id_str = "22536055"
Group By "hour3"
"""

query_tweets_at_aa = """
SELECT SUBSTRING("created_at", 12, 2) AS "hour4", count("hour4") AS amount5
FROM tweets
where AmericanAir_mentioned == 1
Group By "hour4"
"""

df_tweets_per_hour_at_ba = pd.read_sql_query(query_tweets_at_ba, conn)
df_tweets_per_hour_by_ba = pd.read_sql_query(query_tweets_by_ba, conn)
df_tweets_per_hour_at_aa = pd.read_sql_query(query_tweets_at_aa, conn)
df_tweets_per_hour_by_aa = pd.read_sql_query(query_tweets_by_aa, conn)

# add all information into 1 dataframe (received by/sent by)
hour_of_day = ["0-1", "1-2", "2-3", "3-4", "4-5", "5-6", "6-7", "7-8", "8-9", "9-10", "10-11", "11-12", "12-13",
               "13-14", "14-15", "15-16", "16-17", "17-18", "18-19", "19-20", "20-21", "21-22", "22-23", "23-24"]

df_twitter_interaction_british_airways = df_tweets_per_hour_at_ba.copy()
df_twitter_interaction_british_airways["by_ba"] = df_tweets_per_hour_by_ba["amount2"]
df_twitter_interaction_british_airways["hour_of_day"] = hour_of_day
df_twitter_interaction_british_airways.set_index("hour_of_day")

df_twitter_interaction_AmericanAir = df_tweets_per_hour_at_aa.copy()
df_twitter_interaction_AmericanAir["by_aa"] = df_tweets_per_hour_by_aa["amount4"]
df_twitter_interaction_AmericanAir["hour_of_day"] = hour_of_day
df_twitter_interaction_AmericanAir.set_index("hour_of_day")

# free memory space
del df_tweets_per_hour_by_aa, df_tweets_per_hour_by_ba, df_tweets_per_hour_at_aa, df_tweets_per_hour_at_ba
gc.collect()

# creating line plots
fig, ax_hourly = plt.subplots(nrows=2, squeeze=False, figsize=(24, 16))
fig.suptitle("twitter interactions per hour", size=20, weight="bold")

df_twitter_interaction_british_airways.plot(kind="line", ax=ax_hourly[0, 0])
ax_hourly[0,0].set_title("British Airways")
ax_hourly[0,0].set_xlabel("the hour of the day")
ax_hourly[0,0].set_ylabel("amount of tweets")
ax_hourly[0,0].set_xticks(np.arange(len(hour_of_day)), hour_of_day)
ax_hourly[0,0].legend(["tweets send at British airways", "tweets send by British Airways"])

df_twitter_interaction_british_airways.plot(kind="line", ax=ax_hourly[1, 0])
ax_hourly[1,0].set_title("American Air")
ax_hourly[1,0].set_xlabel("the hour of the day")
ax_hourly[1,0].set_xticks(np.arange(len(hour_of_day)), hour_of_day)
ax_hourly[1,0].set_ylabel("amount of tweets")
ax_hourly[1,0].legend(["tweets send at American Air", "tweets send by American Air"])

# show and save the plot
plt.show()
fig.get_figure().savefig("../figures/twitter interactions per hour.svg")


# ----------------------------- comparison of hourly twitter interaction between our airlines --------------------------
# adding data to compare into one dataframe
df_sent_hourly = pd.DataFrame()
df_sent_hourly["american_air"] = df_twitter_interaction_AmericanAir["by_aa"]
df_sent_hourly["British_airways"] = df_twitter_interaction_british_airways["by_ba"]
df_sent_hourly["hour_of_day"] = hour_of_day
df_sent_hourly.set_index("hour_of_day")

df_received_hourly = pd.DataFrame()
df_received_hourly["aa"] = df_twitter_interaction_AmericanAir["amount5"]
df_received_hourly["British_airways"] = df_twitter_interaction_british_airways["amount3"]
df_received_hourly["hour_of_day"] = hour_of_day
df_received_hourly.set_index("hour_of_day")

# free memory space
del df_twitter_interaction_british_airways, df_twitter_interaction_AmericanAir
gc.collect()

# create line plots
fig, ax_hourly2 = plt.subplots(nrows=2, squeeze=False, figsize=(17, 10))
fig.suptitle("comparison of hourly twitter interaction between British Airways and American Air", size=18, weight="bold")
df_sent_hourly.plot(kind="line", ax=ax_hourly2[0, 0])
ax_hourly2[0,0].set_title("tweets sent by airline", size=16, weight="bold")
ax_hourly2[0,0].set_xlabel("the hour of the day", size=13, weight="bold")
ax_hourly2[0,0].set_ylabel("amount of tweets", size=13, weight="bold")
ax_hourly2[0,0].set_xticks(np.arange(len(hour_of_day)), hour_of_day)
ax_hourly2[0,0].legend(["tweets sent by American Air", "tweets sent by British Airways"])

df_received_hourly.plot(kind="line", ax=ax_hourly2[1, 0])
ax_hourly2[1,0].set_title("tweets received by airline", size=16, weight="bold")
ax_hourly2[1,0].set_xlabel("the hour of the day", size=13, weight="bold")
ax_hourly2[1,0].set_ylabel("amount of tweets", size=13, weight="bold")
ax_hourly2[1,0].set_xticks(np.arange(len(hour_of_day)), hour_of_day)
ax_hourly2[1,0].legend(["tweets received by American Air", "tweets received by British Airways"])

plt.subplots_adjust(hspace=0.3)

# show and save the plots
plt.show()
fig.get_figure().savefig("../figures/twitter interactions per hour comparison.svg")

# free memory space
del df_sent_hourly, df_received_hourly


# -------------------- Sent Tweets per airline --------------------
select_list = []
for airline in variables.AIRLINES.keys():
    select_list.append(f"sum({airline}_mentioned) as {airline}")

query_sent_airlines = f"""
SELECT user_id_str
FROM tweets
"""

df_sent = pd.read_sql_query(query_sent_airlines, conn)
dct_sent = {}
for airline in variables.AIRLINES.items():
    dct_sent[airline[0]] = len(df_sent[df_sent["user_id_str"] == str(airline[1])].index)

df_sent = pd.DataFrame(list(dct_sent.items()), columns=["airline", "amount"])

plot_sent = df_sent.plot(kind="bar", x="airline", y="amount")
plot_sent.set_title("Tweets sent per airline")
plt.tight_layout()
plot_sent.get_legend().remove()

plot_sent.get_figure().savefig("../figures/tweets_send.svg")
plt.show()

del df_sent
gc.collect()


# -------------------- Received Tweets per airline --------------------
query_received_airlines = f"""
SELECT {", ".join(select_list)}
FROM tweets
"""

df_received = pd.read_sql_query(query_received_airlines, conn)
dct = {}
for i in df_received:
    dct[i] = df_received[i].iloc[0]
df_received = pd.DataFrame(list(dct.items()), columns=["airline", "amount"])

plot = df_received.plot(kind="bar", x="airline", y="amount")
plot.set_title("Tweets received per airline")
plt.tight_layout()
plot.get_legend().remove()

plt.show()
plot.get_figure().savefig("../figures/Tweets per Airline.svg")

del df_received
gc.collect()


# ------------ Prepare data for Tweets per week (IS NOT RIGHT) ----------------
query_week_british = """
SELECT created_at
FROM tweets
WHERE user_id_str == "18332190"
"""

df_weeks_british = pd.read_sql_query(query_week_british, conn)

dct_week_british = {}
for value in df_weeks_british.values:
    dt = parse(value[0])
    week_number = dt.isocalendar().week
    try:
        dct_week_british[week_number] += 1
    except:
        dct_week_british[week_number] = 1

query_week_american = """
SELECT created_at
FROM tweets
WHERE user_id_str == "22536055"
"""

df_weeks_american = pd.read_sql_query(query_week_american, conn)

dct_week_american = {}
for value in df_weeks_american.values:
    dt = parse(value[0])
    week_number = dt.isocalendar().week
    try:
        dct_week_american[week_number] += 1
    except:
        dct_week_american[week_number] = 1

dct_week = {25: 174184, 26: 135448, 27: 151391, 28: 140758, 21: 120230, 22: 192864, 29: 176109, 30: 122389, 31: 123574,
            32: 362465, 33: 75837, 34: 25167, 35: 127458, 36: 141559, 37: 108549, 38: 139413, 39: 118070, 40: 53016,
            43: 61383, 44: 103078, 45: 108718, 46: 114637, 23: 483666, 47: 157393, 48: 141568, 49: 100776, 50: 108230,
            51: 97700, 52: 83288, 1: 104477, 2: 120943, 3: 90930, 4: 85781, 5: 123465, 6: 118196, 7: 240412, 8: 171964,
            9: 127990, 10: 146721, 11: 283115, 12: 437566, 13: 224616, 14: 41102, 24: 142624} # Hard code because takes too long to load in

query_weeks_total = """
SELECT created_at_datetime
FROM tweets
"""
df_weeks_total = pd.read_sql_query(query_weeks_total, conn)
# df_weeks_total["week_number"] = df_weeks_total["created_at_datetime"].dt.isocalender().week
# print(df_weeks_total)
print(df_weeks_total["created_at_datetime"].iloc[0])
print(type(df_weeks_total["created_at_datetime"].iloc[0]))

# Prepare data for Tweets per week
dct_week = dict(sorted(dct_week.items(), key=lambda item: item[0]))
dct_week_american = dict(sorted(dct_week_american.items(), key=lambda item: item[0]))
dct_week_british = dict(sorted(dct_week_british.items(), key=lambda item: item[0]))

df_week_plot = pd.DataFrame(list(dct_week.items()), columns=["week", "amount"])
df_weeks_american_plot = pd.DataFrame(list(dct_week_american.items()), columns=["week", "amount"])
df_weeks_british_plot = pd.DataFrame(list(dct_week_british.items()), columns=["week", "amount"])

df_week_plot = df_week_plot.rename({"week": "week", "amount": "total_amount"}, axis="columns")
df_weeks_british_plot = df_weeks_british_plot.rename({"week": "week", "amount": "british_amount"}, axis="columns")
df_weeks_american_plot = df_weeks_american_plot.rename({"week": "week", "amount": "american_amount"}, axis="columns")

frames = [df_week_plot, df_weeks_british_plot, df_weeks_american_plot]
result = pd.merge(df_weeks_american_plot, df_weeks_british_plot, on="week")

del df_weeks_british_plot, df_weeks_american_plot
gc.collect()


# --------------- Plot Tweets per week (IS NOT RIGHT) --------------
fig, ax = plt.subplots(nrows=2, ncols=1)

plot_weeks = df_week_plot.plot(kind="line", x="week", ax=ax[0])
ax[0].get_legend().remove()
plot_weeks.set_title("Tweets per week", size=16, weight="bold")
plot_weeks.set_xlabel("Week number")
plot_weeks.set_ylabel("Amount of tweets")

plot_result = result.plot(kind="line", x="week", ax=ax[1])
ax[1].legend(["American Air", "British Airways"])
plot_result.set_xlabel("Week number")
plot_result.set_ylabel("Amount of tweets")
fig.tight_layout()

fig.show()
fig.savefig("../figures/tweet_per_week.svg")
conn.close()