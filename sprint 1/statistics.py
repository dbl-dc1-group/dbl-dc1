import pandas as pd
import pyshark as spark
import sqlite3
import variables
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
# warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)

conn = sqlite3.connect("../database/tweets.db")
cursor = conn.cursor()

sql = "SELECT user_id_str, COUNT() AS airline_sum FROM tweets WHERE user_id_str in ({}) GROUP BY user_id_str".format(", ".join(["?"] * len(variables.AIRLINES)))
cursor.execute(sql, tuple(variables.AIRLINES.values()))
# fetch the results of the query
results = cursor.fetchall()
l = []
# do something with the results
for row in results:
    l.append(row)

end = []
def find_airline(x: int, y: int):
    for zz in variables.AIRLINES:
        if int(x) == int(variables.AIRLINES[zz]):
            end.append((zz, y))
    return end

for z in l:
    find_airline(z[0], z[1])


df_stat = pd.DataFrame(end, columns=["airlines", "replies"])
print(df_stat)

query_retweet_british = """
SELECT British_Airways_mentioned, in_reply_to_screen_name
FROM tweets
WHERE British_Airways_mentioned = 1 AND (in_reply_to_screen_name = "British_Airways" OR in_reply_to_screen_name = "None")
"""
df_tweets_british = pd.read_sql_query(query_retweet_british, conn)
print(f"number of British tweets: {len(df_tweets_british)}")

query_retweet_american = """
SELECT AmericanAir_mentioned, in_reply_to_screen_name
FROM tweets
WHERE AmericanAir_mentioned = 1 AND (in_reply_to_screen_name = "AmericanAir" OR in_reply_to_screen_name = "None")
"""
df_tweets_american = pd.read_sql_query(query_retweet_american, conn)
print(f"number of American tweets: {len(df_tweets_american)}")


# Set the data
british_replies = df_stat[df_stat["airlines"] == "British_Airways"]
british_tweets = len(df_tweets_british)
american_replies = df_stat[df_stat["airlines"] == "AmericanAir"]
american_tweets = len(df_tweets_american)
for entry in american_replies["replies"]:
    american_reply = entry
for entry in british_replies["replies"]:
    british_reply = entry
print(f"The ratio of replies of British Airways is {british_reply/british_tweets }")
print(f"The ratio of replies of AmericanAir is {american_reply/american_tweets }")
# Create a figure and axis object
fig, ax = plt.subplots()

# Set the width of each bar
width = 1

# Create two sets of bars side by side
# ax.bar([0, width*2], [british_tweets, british_reply], width, label="British")
# ax.bar([width*4, width*6], [american_tweets, american_reply], width, label="American")

ax.bar([0, width*2], [british_tweets, american_tweets], width, label="received by airline")
ax.bar([width*4, width*6], [british_reply, american_reply], width, label="sent by airline")


# Set the x-axis tick labels
ax.set_xticks([0, width*2, width*4, width*6])
# ax.set_xticklabels(["British Tweets", "British replies" , "American Tweets", "American replies"])
ax.set_xticklabels(["British Airways Tweets", "AmericanAir Tweets", "British Airways replies", "AmericanAir replies"])

# Set the y-axis label and title
ax.set_ylabel("Count", weight="bold")
ax.set_title("Comparison of Tweets and replies", size=16, weight="bold")

# Add a legend
ax.legend()

# Show the plot
plt.xticks(rotation=-45)
plt.tight_layout()
plt.show()
fig.savefig("figures/statistics.svg")
