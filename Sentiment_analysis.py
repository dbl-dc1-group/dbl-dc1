import pandas as pd
import time
import sqlite3
from transformers import RobertaTokenizer
from transformers import TFRobertaForSequenceClassification
from transformers import RobertaConfig
tokenizer = RobertaTokenizer.from_pretrained("roberta-base")
pretrained_config = RobertaConfig("roberta-base")
model = TFRobertaForSequenceClassification.from_pretrained("roberta-base")
sentiment = ["very negative","negative","neutral","positive","very positive"]
conn = sqlite3.connect("database/tweets.db")
#SA is short for sentiment analysis
query_for_SA = """
SELECT text 
FROM tweets
WHERE British_Airways_mentioned == 1
"""

df_tweets_at_ba = pd.read_sql_query(query_for_SA, conn)
df_tweets_at_ba_copy = df_tweets_at_ba.copy(deep= True)
df_tweets_for_testing_code = df_tweets_at_ba_copy.iloc[:40]
t = time.time()
begin_time = int(t*1000)
classified_tweets = []
for i in df_tweets_for_testing_code["text"]:
    tokenized_tweet = tokenizer(i, return_tensors="tf")
    classified_tweet = model(tokenized_tweet)
    classified_tweets.append(classified_tweet)
t = time.time()
end_time = int(t*1000)
print(classified_tweets)
print(f"time took:{((end_time-begin_time)/1000):.2f} seconds")