import sqlite3
import pandas as pd
import os

try:
    os.remove("database/tweets.db")
except Exception as e:  # File was already deleted
    print(f"Error: {e}")

parquet_path = "database/airlines_db.parquet"
table_name = "tweets"
query = f"""
SELECT *
FROM {table_name}
"""
conn = sqlite3.connect("database/tweets.db")

df_parquet = pd.read_parquet(parquet_path)
df_parquet = df_parquet.drop_duplicates("id_str")
num_rows_inserted = df_parquet.to_sql(table_name, conn, index=False)
cursor = conn.execute(query)