import os
import pandas as pd
from variables import LENGTH_JSON


def check_files():
    df = pd.read_parquet("database/airlines_db.parquet")
    length = len(df.index)
    del df
    print(
        f"Are the lengths of the parquet and the jsons equal? {LENGTH_JSON == length} (Length json: {LENGTH_JSON}; Length parquet database: {length})")
