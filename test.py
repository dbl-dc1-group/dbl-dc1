import pandas as pd
import sqlite3
import time

conn = sqlite3.connect('database/tweets.db')

query = """
SELECT in_reply_to_status_id_str, id_str
FROM tweets
WHERE in_reply_to_status_id_str != 'None'
"""

df = pd.read_sql_query(query, conn)

ids_replies = list(df['in_reply_to_status_id_str'].values)
count = 0
for tweet_id in list(df['id_str'].values):
    if tweet_id not in ids_replies:
        count += 1
        print(tweet_id)

print(count)
print(ids_replies)