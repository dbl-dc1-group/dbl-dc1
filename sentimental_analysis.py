from nltk.sentiment import SentimentIntensityAnalyzer
import operator
import sqlite3
import pandas as pd
import numpy as np
import nltk
nltk.download("vader_lexicon")
import time

# sia = SentimentIntensityAnalyzer()
# df["sentiment_score"] = df["reviews.text"].apply(lambda x: sia.polarity_scores(x)["compound"])
# df["sentiment"] = np.select([df["sentiment_score"] < 0, df["sentiment_score"] == 0, df["sentiment_score"] > 0],
#                            ["neg", "neu", "pos"])


conn = sqlite3.connect("database/tweets.db")

query_sentimental_analysis_british = """
SELECT text
FROM tweets
WHERE British_Airways_mentioned == "1" AND lang == "es" AND in_reply_to_status_id_str == "None" AND in_reply_to_user_id_str == "None"
"""

t = time.time()
begin_time = int(t * 1000)

sia = SentimentIntensityAnalyzer()
df_sentimental_analysis_british = pd.read_sql_query(query_sentimental_analysis_british, conn)
df_sentimental_analysis_british["sentiment_score"] = df_sentimental_analysis_british["text"].apply(lambda x: sia.polarity_scores(x)["compound"])
df_sentimental_analysis_british["sentiment"] = np.select([df_sentimental_analysis_british["sentiment_score"] < 0, df_sentimental_analysis_british["sentiment_score"] == 0, df_sentimental_analysis_british["sentiment_score"] > 0],
                                                         ["neg", "neu", "pos"])
t = time.time()
end_time = int(t * 1000)
time_took = (end_time-begin_time)/1000
print(f"Took {time_took:.2f} seconds ({(time_took/len(df_sentimental_analysis_british.index)):.8f} seconds per line)")

df_sentimental_analysis_british.to_parquet("database/sentimental_british_es.parquet")
print(df_sentimental_analysis_british["sentiment_score"].mean())
