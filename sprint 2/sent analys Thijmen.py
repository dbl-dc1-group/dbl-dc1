import pandas as pd
import sqlite3
from textblob import TextBlob
import torch
from transformers import AutoModel, AutoTokenizer
import nltk
nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer

conn0 = sqlite3.connect("../database/tweets.db")
conn1 = sqlite3.connect("C:/Users/20223105/Downloads/archive (1)/database.sqlite")

q0 = """
SELECT text AS tweet, airline_sentiment
FROM Tweets
"""

df_0 = pd.read_sql_query(q0, conn1)
print(df_0)

# --------------------------------------------vader---------------------------------------------------------------------
sid = SentimentIntensityAnalyzer()

df_0['scores'] = df_0['tweet'].apply(lambda review: sid.polarity_scores(review))
print(df_0)

df_0['compound']  = df_0['scores'].apply(lambda score_dict: score_dict['compound'])
print(df_0)

# df_0['comp_score'] = df_0['compound'].apply(lambda c: 'positive' if c > 0 "negative" elif c < 0 else 'neg')
def getAnalysis(score):
    if score < 0:
        return "negative"
    elif score == 0:
        return "neutral"
    else:
        return "positive"


df_0["comp_score"] = df_0["compound"].apply(getAnalysis)
print(df_0)

print(f"This method is {(100*len(df_0[df_0['airline_sentiment'] == df_0['comp_score']].index))/len(df_0.index):.2f}% correct")


# -------------------------------------------------bertweet (doesn't work)----------------------------------------------
# bertweet = AutoModel.from_pretrained("vinai/bertweet-large")
# tokenizer = AutoTokenizer.from_pretrained("vinai/bertweet-large", use_fast=False)
# a = torch.tensor(tokenizer.encode(df_0["tweet"].iloc[0]))
#
# # from transformers import TFAutoModel
# # bertweet = TFAutoModel.from_pretrained("vinai/bertweet-base")
#
# print(df_0["tweet"].iloc[0])
# print(a)
# features = a.unsqueeze(0)


# with torch.no_grad():
#     features = bertweet(a)
# print(features)


# ---------------------------------------------------TextBlob-----------------------------------------------------------
# def sentiment_analysis(tweet):
#     def getSubjectivity(text):
#         return TextBlob(text).sentiment.subjectivity
#
#     # Create a function to get the polarity
#     def getPolarity(text):
#         return TextBlob(text).sentiment.polarity
#
#     # Create two new columns ‘Subjectivity’ & ‘Polarity’
#     tweet["TextBlob_Subjectivity"] = tweet["tweet"].apply(getSubjectivity)
#     tweet["TextBlob_Polarity"] = tweet["tweet"].apply(getPolarity)
#
#     def getAnalysis(score):
#         if score < 0:
#             return "Negative"
#         elif score == 0:
#             return "Neutral"
#         else:
#             return "Positive"
#
#     tweet["TextBlob_Analysis"] = tweet["TextBlob_Polarity"].apply(getAnalysis)
#     return tweet
#
# def getSubjectivity(text):
#     return TextBlob(text).sentiment.subjectivity
#
#
# df_0["TextBlob_Subjectivity"] = df_0["tweet"].apply(getSubjectivity)
#
#
# def getPolarity(text):
#     return TextBlob(text).sentiment.polarity
#
#
# df_0["TextBlob_Polarity"] = df_0["tweet"].apply(getPolarity)
#
#
# def getAnalysis(score):
#     if score < 0:
#         return "negative"
#     elif score == 0:
#         return "neutral"
#     else:
#         return "positive"
#
#
# df_0["TextBlob_Analysis"] = df_0["TextBlob_Polarity"].apply(getAnalysis)
# # print(df_0)
# # print(df_0[df_0["airline_sentiment"] == df_0["TextBlob_Analysis"]])
# print(f"This method is {(100*len(df_0[df_0['airline_sentiment'] == df_0['TextBlob_Analysis']].index))/len(df_0.index):.2f}% correct")
