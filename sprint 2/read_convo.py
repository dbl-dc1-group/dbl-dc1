counts = {}

with open("convo.txt", "r") as file:
    for line in file:
        key, values = line.strip().split(":")
        values = values.strip().split(",")
        count = len(values)
        counts[key] = count

# Sort the counts in descending order
sorted_counts = {k: v for k, v in sorted(counts.items(), key=lambda item: item[1], reverse=True)}

# Print the sorted counts
for key, count in sorted_counts.items():
    print(f"{key}: {count}")
