import datetime
import sqlite3
import pandas as pd
from dateutil import parser
import matplotlib.pyplot as plt
import variables
from variables import AIRLINES
import time

conn = sqlite3.connect("../database/tweets.db")
list_id = []
data = open("convo 1.txt")
for id in data:
    list_id.append(str.strip(id))
query = """
SELECT id_str
FROM tweets
WHERE id_str IN (SELECT in_reply_to_status_id_str FROM tweets)
Order By created_at_datetime ASC
"""

df_convo = pd.read_sql_query(query, conn)
number_of_convos = len(df_convo.index)
convo_dict = {}
query2 = f"""
        SELECT id_str, in_reply_to_status_id_str, created_at_datetime
        FROM tweets
        WHERE in_reply_to_status_id_str <> 'None'
        Order By created_at_datetime ASC
        """
df_convo2 = pd.read_sql_query(query2, conn)
def create_convo(id, original):
    if df_convo2[df_convo2['in_reply_to_status_id_str'] == id].empty:
        return convo_dict
    else:
        if original == True:
            # row_id = df_convo2[df_convo2['in_reply_to_status_id_str'] == id]
            convo_dict[id] = [id]
            return create_convo(id, False)
        else:
            row_id = df_convo2[df_convo2['in_reply_to_status_id_str'] == id]
            convo_dict[id].append(row_id['id_str'].iloc[0])
            convo_dict[row_id['id_str'].iloc[0]] = convo_dict[row_id['in_reply_to_status_id_str'].iloc[0]]
            del convo_dict[id]
            return create_convo(row_id['id_str'].iloc[0], False)
count = 0
time_took_list = []
list_ids = list(df_convo['id_str'].values)
for entry in df_convo['id_str']:
    t = time.time()
    begin_time = int(t*1000)
    create_convo(entry, True)
    count += 1
    t = time.time()
    end_time = int(t*1000)
    time_took = (end_time-begin_time)/1000
    time_took_list.append(time_took)
    print(f'Done {count} conversations, took {time_took:.2f} seconds (around {number_of_convos-count} to go) (still {(((number_of_convos-count)*(sum(time_took_list)/len(time_took_list)))/3600):.2f}) hours to go')
print(convo_dict)
