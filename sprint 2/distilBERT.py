import pandas as pd
import time
import sqlite3
from transformers import DistilBertTokenizer
from transformers import TFDistilBertForSequenceClassification
import tensorflow as tf
import numpy as np


if tf.test.is_gpu_available():
    # Set TensorFlow to use GPU
    tf.config.experimental.set_memory_growth(tf.config.list_physical_devices('GPU')[0], True)
#from transformers import DistilBertConfig
conn = sqlite3.connect("../database/database.sqlite")
#configuration = DistilBertConfig.from_pretrained("distilbert-base-cased")
tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-cased")
model = TFDistilBertForSequenceClassification.from_pretrained("distilbert-base-cased")

query_for_SA = """
SELECT text, airline_sentiment 
FROM Tweets
"""

df_tweets_at_ba = pd.read_sql_query(query_for_SA, conn)
df_tweets_at_ba_copy = df_tweets_at_ba.copy(deep= True)
df_tweets_for_testing_code = df_tweets_at_ba_copy.iloc[:50]
t = time.time()
begin_time = int(t*1000)
classified_tweets1 = []
classified_tweets2 = []

for i in df_tweets_for_testing_code["text"]:
    tokenized_tweet = tokenizer(i, return_tensors="tf")
    classified_tweet = model(tokenized_tweet)
    classified_tweets1.append(classified_tweet)
t = time.time()
end_time = int(t*1000)
print(classified_tweets1)
print(f"time took:{((end_time-begin_time)/1000):.2f} seconds")

# Define the sentiment labels
sentiment_labels = ["Negative", "Neutral", "Positive"]

# Set the threshold for positive and negative sentiments
positive_threshold = 0.7
negative_threshold = 0.3

# Convert BERT output to sentiment labels
sentiment_results = []
for tweet_classification in classified_tweets1:
    probabilities = tweet_classification.logits[0]
    sentiment_probability = tf.nn.softmax(probabilities).numpy()
    sentiment_index = np.argmax(sentiment_probability)
    #print(sentiment_probability[sentiment_index])
    if sentiment_probability[sentiment_index] >= positive_threshold:
        sentiment_label = sentiment_labels[2]  # Positive
    elif sentiment_probability[sentiment_index] <= negative_threshold:
        sentiment_label = sentiment_labels[0]  # Negative
    else:
        sentiment_label = sentiment_labels[1]  # Neutral

    sentiment_results.append(sentiment_label)

# Print the sentiment labels for each tweet
df_compare_result = pd.DataFrame(columns=["tweet", "sentiment", "original"])

i=0
for tweet, sentiment in zip(df_tweets_for_testing_code["text"], sentiment_results):
    print(sentiment, df_tweets_at_ba_copy['airline_sentiment'].iloc[i])
    i += 1



