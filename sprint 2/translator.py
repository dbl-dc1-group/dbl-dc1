import pandas as pd
from deep_translator import GoogleTranslator
import sqlite3
import time
import gc

# retweets False if no retweets, retweets true if you only want retweets
retweets = True


# connect to SQLite database
conn = sqlite3.connect("../database/tweets.db")
cursor = conn.cursor()

if not retweets:
    # extract all non-english tweets where at least one airway is mentioned (no duplicates/retweets)
    query_translator = """
    SELECT id_str, text AS text_original, lang AS lang_original
    FROM tweets
    WHERE lang_original != "und" AND retweeted == "False" AND is_duplicate_of_id == "None" AND lang_original != "en" AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
    """

elif retweets:
    # extract all non-english tweets where at least one airway is mentioned (no duplicates/with retweets)
    query_translator = """
    SELECT id_str, text AS text_original, lang AS lang_original
    FROM tweets
    WHERE lang_original != "und" AND retweeted == "True" AND is_duplicate_of_id == "None" AND lang_original != "en" AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
    """

df_translator = pd.read_sql_query(query_translator, conn)


# setting default variables
length_df = len(df_translator.index)
lst_text_en = []

# count means the starting row, stop_count = "none" if you want to keep going, otherwise fill your end row in.
count = 0
stop_count = "none"
count_error = 0

# df_translated = df_translator[count:].copy()
df_translator = df_translator[count:].copy()

t = time.time()
begin_time = int(t * 1000)
real_begin_time = begin_time

df_1 = pd.DataFrame()

# basic checks:
if count % 1000 != 0:
    raise Exception("'count' must be dividable by 1000")

try:
    if stop_count % 1000 != 0 and stop_count != "none":
        raise Exception("'stop_count' must be dividable by 1000")
except:
    pass


# iterating over all rows
for row in df_translator.iterrows():
    count += 1

    # try to translate the tweets
    try:
        lst_text_en.append(GoogleTranslator(source="auto", target="en").translate(str(row[1]["text_original"])))
    except:
        lst_text_en.append(row[1]["text_original"])
        count_error += 1
        print(f"failed to translate on line {count-1}")

    if count % 100 == 0 or count == length_df:
        # calculating time to go
        t = time.time()
        end_time = int(t * 1000)
        time_took = (end_time - begin_time) / 1000
        print(f"{count} rows translated. Iterations took {time_took:.2f} seconds ({((((length_df - count) / 100) * time_took) / 3600):.2f} hours to go) ({((100 * count) / length_df):.3f}% done)")
        begin_time = end_time

        # set all translated tweets into 1 dataframe
        df_0 = pd.DataFrame()
        df_0["translation"] = lst_text_en
        df_1 = pd.concat([df_1, df_0], ignore_index=True)

        # free memory space
        del lst_text_en, df_0
        gc.collect()

        df_0 = pd.DataFrame()
        lst_text_en = []

        # save the dataframe in a parquet file (every 1000 tweets)
        if count % 1000 == 0 or count == length_df:
            if count_error == 0:
                if not retweets:
                    df_1.to_parquet(f"../parquet2/translation{count}.parquet")
                elif retweets:
                    df_1.to_parquet(f"../parquet2/RT_translation{count}.parquet")
            else:
                if not retweets:
                    df_1.to_parquet(f"../parquet2/translation{count}_error{count_error}.parquet")
                elif retweets:
                    df_1.to_parquet(f"../parquet2/RT_translation{count}_error{count_error}.parquet")
                count_error = 0

            # free memory space
            del df_1
            gc.collect()

            df_1 = pd.DataFrame()

        if stop_count == count:
            break
print("done!")