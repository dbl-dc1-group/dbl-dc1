import pandas as pd
from deep_translator import GoogleTranslator
import sqlite3

# connect to SQLite database
conn = sqlite3.connect("../database/tweets.db")

# ---------------------------------------------without retweets---------------------------------------------------------
# setting base values
directory = "../parquet2"
lst_directory = []
count = 0
df_translations = pd.DataFrame()

# create a list of directory's so that the order is right
for i in range(186):
    if count < 185000:
        count += 1000
    else:
        count = 185303
    lst_directory.append(f"{directory}/translation{str(count)}.parquet")

# create one dataframe with all translations (without retweets)
for filename in lst_directory:
    df_trans_temp = pd.read_parquet(filename)
    df_translations = pd.concat([df_translations, df_trans_temp], ignore_index=True)

# combine original data with the translated data
query_no_retweets = """
SELECT id_str, text AS text_original, lang AS lang_original
FROM tweets
WHERE lang_original != "und" AND retweeted == "False" AND is_duplicate_of_id == "None" AND lang_original != "en" AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
"""
df_no_retweets = pd.read_sql_query(query_no_retweets, conn)
df_no_retweets["translation"] = df_translations["translation"]


# ---------------------------------------------only retweets---------------------------------------------------------
# setting base values
lst_directory = []
count = 0
df_translations = pd.DataFrame()

# create a list of directory's so that the order is right
for i in range(21):
    if count < 20000:
        count += 1000
    else:
        count = 20824
    lst_directory.append(f"{directory}/RT_translation{str(count)}.parquet")

# create one dataframe with all translations (without retweets)
for filename in lst_directory:
    df_trans_temp = pd.read_parquet(filename)
    df_translations = pd.concat([df_translations, df_trans_temp], ignore_index=True)

# combine original data with the translated data
query_no_retweets = """
SELECT id_str, text AS text_original, lang AS lang_original
FROM tweets
WHERE lang_original != "und" AND retweeted == "True" AND is_duplicate_of_id == "None" AND lang_original != "en" AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
"""
df_retweets = pd.read_sql_query(query_no_retweets, conn)
df_retweets["translation"] = df_translations["translation"]


# ---------------------------------------------only duplicates----------------------------------------------------------
# setting base values
lst_duplicates = []
count = 0

# extract duplicates
query_duplicates = """
SELECT id_str, text AS text_original, lang AS lang_original
FROM tweets
WHERE lang_original != "und" AND retweeted == "False" AND is_duplicate_of_id != "None" AND lang_original != "en" AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
"""

df_duplicates = pd.read_sql_query(query_duplicates, conn)

# iterate over duplicates and search the translation in the earlier created dataframe
for row in df_duplicates["text_original"]:
    count += 1
    duplicate = df_no_retweets[df_no_retweets["text_original"] == row]
    lst_duplicates.append(duplicate["translation"].iloc[0])
    if count % 100 == 0 or count == len(df_duplicates):
        print(f"combining translation with duplicates: {100 * count / len(df_duplicates):.2f}% done.")

# add duplicates to the original data
df_duplicates["translation"] = lst_duplicates

# combine all above created dataframes into one dataframe
df_tweets_en = pd.concat([df_no_retweets, df_retweets, df_duplicates], ignore_index=True)


# ---------------------------------------------duplicates and retweets--------------------------------------------------
# set base values
lst_duplicates_RT = []
count = 0

# extract duplicates and retweets- (intersect)
query_duplicates_RT = """
SELECT id_str, text AS text_original, lang AS lang_original
FROM tweets
WHERE lang_original != "und" AND retweeted == "True" AND is_duplicate_of_id != "None" AND lang_original != "en" AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
"""

df_duplicates_RT = pd.read_sql_query(query_duplicates_RT, conn)

# find translations of the text, if there is none, translate it here
for row in df_duplicates_RT["text_original"]:
    count += 1
    try:  # search for translation
        duplicate_RT = df_tweets_en[df_tweets_en["text_original"] == row]
        lst_duplicates_RT.append(duplicate_RT["translation"].iloc[0])
    except:
        try:  # translate data
            lst_duplicates_RT.append(GoogleTranslator(source="auto", target="en").translate(str(row)))
        except:  # in case of error
            lst_duplicates_RT.append(row)
            print(f"failed to translate on line {count - 1}")
    if count % 100 == 0 or count == len(df_duplicates_RT):
        print(f"combining translation with duplicates and retweets: {100 * count / len(df_duplicates_RT):.2f}% done.")

# put translations with its original data
df_duplicates_RT["translation"] = lst_duplicates_RT


# combine all dataframes into 1 dataframe with id_str as key, and the translation as value
df_tweets_en = pd.concat([df_tweets_en, df_duplicates_RT], ignore_index=True)[["id_str", "translation"]]

# save dataframe as parquet file
df_tweets_en.to_parquet("../database/tweets_en.parquet")


# transfer the parquet file into SQLite (tweets_en table)
print("transferring parquet file into SQlite")
parquet_path = "../database/tweets_en.parquet"
table_name = "tweets_en"
query = f"""
SELECT *
FROM {table_name}
"""

df_parquet = pd.read_parquet(parquet_path)
num_rows_inserted = df_parquet.to_sql(table_name, conn, index=False)
cursor = conn.execute(query)