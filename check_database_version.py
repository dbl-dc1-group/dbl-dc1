import hashlib

NEWEST_HASH = "33864d66a5824ce7779f7e1f47ee09a96766a3be"
DRIVE_LINK = "https://drive.google.com/drive/folders/1AdLu7XbVeOoxsJJ74vYXba8hYEkZIPJD?usp=sharing"


def hash_file(filename):
    """"This function returns the SHA-1 hash
   of the file passed into it"""

    # make a hash object
    h = hashlib.sha1()

    # open file for reading in binary mode
    with open(filename, 'rb') as file:
        # loop till the end of the file
        chunk = 0
        while chunk != b'':
            # read only 1024 bytes at a time
            chunk = file.read(1024)
            h.update(chunk)

    # return the hex representation of digest
    return h.hexdigest()


hash_local_database = hash_file("database/tweets.db")
if hash_local_database == NEWEST_HASH:
    print("You have the newest version of the database!")
else:
    raise Exception(f"Download newest database version from {DRIVE_LINK}")
