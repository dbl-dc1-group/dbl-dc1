import uuid
import pandas as pd
import os
import variables
import json
import time


def write_stats(begin_time: int, end_time: int, count: int, errors: list[str], chunk_size: int, save_path: str,
                tweet_count: int):
    count_file = 0
    for file in os.listdir(save_path):
        if file != ".DS_Store":
            count_file += 1
    time_took = (end_time - begin_time) / 1000
    filename_stats = f"stats/stats-{end_time}.txt"
    errors_newline = "\n".join(errors)
    df_stats = pd.DataFrame({"tweet_count": str(tweet_count)}, index=[0])
    df_stats.to_parquet("stats/stats.parquet")
    if len(errors) <= 2584:
        error_comment = "fine"
    else:
        error_comment = f"probably too much, in general 2584 errors, now {len(errors) - 2584} too much"
    with open(filename_stats, "w") as f:
        f.write(
            f"Started at: {begin_time} - ended at {end_time} -> took {time_took:.2f} seconds ({(time_took / chunk_size):.2f} seconds per iteration)\nNumber of source files: {count} (exported: {count_file} files)\nNumber of tweets: {tweet_count}\nChunk size: {chunk_size}\nNumber of errors: {len(errors)} ({error_comment})\nError lines:\n{errors_newline}")


def write_json(new_data, filename="convs/conv_test.json"):
    with open(filename, "r+") as file:
        # First we load existing data into a dict.
        file_data = json.load(file)
        # Join new_data with file_data inside emp_details
        file_data["convs"].append(new_data)
        # Sets file"s current position at offset.
        file.seek(0)
        # convert back to json.
        json.dump(file_data, file, indent=4)


def conversation(cleaned_tweet: dict):
    conversation_dct = {}
    if cleaned_tweet["in_reply_to_status_id_str"] == None and cleaned_tweet["retweeted"] == False:
        conversation_dct
        conversation_dct["conv_id"] = str(uuid.uuid1())
        conversation_dct["parent_tweet_id"] = cleaned_tweet["id_str"]
        conversation_dct["replies"] = [{}]
        conversation_dct["replies"][0]["reply_index"] = "0"
        conversation_dct["replies"][0]["text"] = cleaned_tweet["text"]
        conversation_dct["replies"][0]["tweet_id"] = cleaned_tweet["id_str"]
        conversation_dct["replies"][0]["user_id_str"] = cleaned_tweet["user_id_str"]
        variables.conversations["convs"].append(json.dumps(conversation_dct))
    else:
        pass
    if conversation_dct != {}:
        write_json(conversation_dct)
